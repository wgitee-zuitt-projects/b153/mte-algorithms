let collection = [];

// Write the queue functions below.
// No ES6/Array methods allowed (anything with a .method() syntax)
// .length is allowed as it is a property, not a method

function print() {
	return collection;
}

function enqueue(element) {
	collection[collection.length] = element;
	return collection;
}

function dequeue() {
	collection.shift();
	return collection;
}

function front() {
	return collection[0];
}

function isEmpty() {
	return collection.length === 0;
}

function size() {
	return collection.length;
}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};